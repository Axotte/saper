import java.util.ArrayList;
import java.util.Random;

public class Field {
    private ArrayList<Mine> mines = new ArrayList<>();
    private int amountOfMines;
    private int sizeOfField;

    public Field(int lvl) {
        switch (lvl) {
            case 1:
                sizeOfField = 8;
                amountOfMines = 10;
                break;
            case 2:
                sizeOfField = 12;
                amountOfMines = 30;
                break;
            case 3:
                sizeOfField = 16;
                amountOfMines = 80;
                break;
        }
        setMines();
    }

    private void setMines() {
        ArrayList<Integer> pozycja = new ArrayList<>();
        Random generator = new Random();
        int i = 0;
        while (i < amountOfMines) {
            int j = generator.nextInt(sizeOfField * sizeOfField);
            if (pozycja.indexOf(j) < 0) {
                pozycja.add(j);
                System.out.println(j);
                mines.add(new Mine(j / sizeOfField, j % sizeOfField));
                i++;
            }
        }
    }

    boolean isMine(int x, int y) {
        for(Mine i: mines) {
            int xm = i.getX();
            int ym = i.getY();
            if (x == xm && y == ym) {
                return true;
            }
        }
        return false;
    }

    int getAmountOfSurroundingMines(int x, int y) {
        int amount = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if(i == 0 && j == 0) {
                    continue;
                }
                if (isMine(x + i, y + j)) {
                    amount++;
                }
            }
        }
        return amount;
    }
}
