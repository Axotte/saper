import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Frame extends JFrame {

    private static final String EASY = "minesweeper.easy";
    private static final String MEDIUM = "minesweeper.medium";
    private static final String HARD = "minesweeper.hard";

    private int width = 400;
    private int height = 400;
    private int lvl = 1;
    private int amountOfMines;
    private int sizeOfField;
    private Field field;
    private int toFinish;
    private boolean isFirst = true;

    private ArrayList<JButton> buttons = new ArrayList<>();

    public Frame() {
        super("Saper");
        setMinesAndFields();
        field = new Field(lvl);
        setButtons();
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setSize(width + getInsets().left + getInsets().right, height + getInsets().top + getInsets().bottom);
        setVisible(true);
    }

    private void setMinesAndFields() {
        switch (lvl) {
            case 1:
                sizeOfField = 8;
                amountOfMines = 10;
                break;
            case 2:
                sizeOfField = 12;
                amountOfMines = 30;
                break;
            case 3:
                sizeOfField = 16;
                amountOfMines = 80;
                break;
        }
    }

    private void setButtons() {
        buttons.clear();
        getContentPane().removeAll();
        JPanel centerPanel = new JPanel(new GridLayout(sizeOfField, sizeOfField));
        for (int i = 0; i < sizeOfField * sizeOfField; i++) {
            JButton but = new JButton();
            but.addActionListener(new FieldActionListener(i));
            centerPanel.add(but);
            buttons.add(but);
        }

        JPanel topPanel =  new JPanel(new GridLayout());

        JButton butEasy = new JButton("Łatwy");
        JButton butMedium = new JButton("Średni");
        JButton butHard = new JButton("Trudny");

        butEasy.addActionListener(new LvlActionListener(EASY));
        butMedium.addActionListener(new LvlActionListener(MEDIUM));
        butHard.addActionListener(new LvlActionListener(HARD));

        topPanel.add(butEasy);
        topPanel.add(butMedium);
        topPanel.add(butHard);
        getContentPane().add(BorderLayout.CENTER, centerPanel);
        getContentPane().add(BorderLayout.NORTH, topPanel);
        toFinish = sizeOfField * sizeOfField - amountOfMines;
    }

    private class FieldActionListener implements ActionListener {
        private int index;

        public FieldActionListener(int index) {
            this.index = index;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println(isFirst);
            if (field.isMine(index / sizeOfField, index % sizeOfField)) {
                if (isFirst) {
                    while (true) {
                        if (field.isMine(index / sizeOfField, index % sizeOfField)) {
                            changeLvl(lvl);
                        } else {
                            break;
                        }
                    }
                    isFirst = false;
                    check(index / sizeOfField, index % sizeOfField);
                    return;
                }
                isFirst = false;
                System.out.println(true);
                for (int i = 0; i < sizeOfField * sizeOfField; i++) {
                    if (field.isMine(i / sizeOfField, i % sizeOfField)) {
                        buttons.get(i).setText("M");
                        buttons.get(i).setEnabled(false);
                        buttons.get(i).setBackground(Color.RED);
                    } else {
                        buttons.get(i).setEnabled(false);
                        if (!(field.getAmountOfSurroundingMines(i / sizeOfField, i % sizeOfField) == 0)) {
                            buttons.get(i).setText(Integer
                                    .toString(field.getAmountOfSurroundingMines(i / sizeOfField, i % sizeOfField)));
                        }
                    }
                }
            } else {
                isFirst = false;
                check(index / sizeOfField, index % sizeOfField);
            }
        }
        private void check(int x, int y) {
            if (buttons.get(x * sizeOfField + y).isEnabled()) {
                toFinish--;
                if (toFinish == 0) {
                    for (int i = 0; i < sizeOfField * sizeOfField; i++) {
                        if (field.isMine(i / sizeOfField, i % sizeOfField)) {
                            buttons.get(i).setText("M");
                            buttons.get(i).setEnabled(false);
                            buttons.get(i).setBackground(Color.CYAN);
                        }
                    }
                }
                System.out.println(toFinish);
                buttons.get(x * sizeOfField + y).setEnabled(false);
                if (field.getAmountOfSurroundingMines(x, y) == 0) {
                    for (int i = -1; i <= 1; i++) {
                        for (int j = -1; j <= 1; j++) {
                            if ((x + i) >= 0 &&(x + i) < sizeOfField && (y + j) >= 0 &&(y + j) < sizeOfField) {
                                check(x + i, y + j);
                            }
                        }
                    }
                } else {
                    buttons.get(x * sizeOfField + y).setEnabled(false);
                    buttons.get(x * sizeOfField + y).setText(Integer.toString(field.getAmountOfSurroundingMines(x, y)));
                }
            }
        }
    }

    private class LvlActionListener implements ActionListener {

        private String difficulty;

        public LvlActionListener(String difficulty) {
            this.difficulty = difficulty;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            switch (difficulty) {
                case EASY:
                    changeLvl(1);
                    break;
                case MEDIUM:
                    changeLvl(2);
                    break;
                case HARD:
                    changeLvl(3);
                    break;
            }
        }
    }

    void changeLvl(int lvl) {
        isFirst = true;
        this.lvl = lvl;
        switch (lvl) {
            case 1:
                this.width = 400;
                if (this.height == 400)
                    this.height = 401;
                else
                    this.height = 400;
                break;
            case 2:
                this.width = 550;
                if (this.height == 550)
                    this.height = 551;
                else
                    this.height = 550;
                break;
            case 3:
                this.width = 750;
                if (this.height == 700) {
                    this.height = 701;
                } else {
                    this.height = 700;
                }
                break;
        }
        setSize(width + getInsets().left + getInsets().right, height + getInsets().top + getInsets().bottom);


        setMinesAndFields();
        setButtons();
        field = new Field(lvl);
    }
}
